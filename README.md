# MS-RANAS: Multi-Scale Resource-Aware Neural Architecture Search

Author: Cristian Cioflan <cioflanc@student.ethz.ch> Copyright (C) 2021 ***ETH Zürich***. All rights reserved.

**Citing**
If you use MS-RANAS in an academic or industrial context, please cite the following publication:

*MS-RANAS: Multi-Scale Resource-Aware Neural Architecture Search* [arXiv preprint](https://arxiv.org/abs/2009.13940)

~~~~
@article{Cioflan2020MSRANASMR,
  title={MS-RANAS: Multi-Scale Resource-Aware Neural Architecture Search},
  author={Cristian Cioflan and R. Timofte},
  journal={ArXiv},
  year={2020},
  volume={abs/2009.13940}
}
~~~~

## 1. Introduction

**MS-RANAS** is a neural architecture search framework for efficient anytime inference in image classification tasks. Feature maps of multiple sizes are kept simultaneously in the network at each layer to provide coarse- and fine-grained information. Similarily, each layer contains an early exits, offering an accurate result for a certain input image without traversing the network's entire depth. Within each layer, the information provided by feature maps of different sizes is combined and processed by a cell, whose structure learnt by our one-shot neural architecture search approach.

## 2. Dependencies

A suitable environment can be created by running the following command:

~~~~shell
$ conda create --name <env> --file requirements.txt
~~~~

## 3. MS-RANAS Configurations

Using the arguments described in `arguments.py`, we provide the following examples:

### Searching for an optimal cell structure on CIFAR10:

```
python3 main.py --search True --save /path/to/file --nBlocks 5 --growthFactor 1-2 --seed 2 --nClassifiers 5 --classifiers 1-2-3-4-5 --cutout --data cifar10

```

### Training a network containing a predefined/searched genotype on CIFAR100:

```
python3 main.py  --save /path/to/file --arch genotype --epochs 600 --nBlocks 7 --nChannels 10 --growthFactor 1-2-4 --classifiers 2-3-4-5-6-7 --batch-size 96 --seed 0 --drop-path-prob 0.2 --data cifar100 --cutout --gpu 1

```

### Evaluating a pretrained model on CIFAR10 for anytime inference:

```
python3 main.py  --save /path/to/file --evaluate-from  model_cifar10/ --arch genotype --nBlocks 7 --nChannels 10 --growthFactor 1-2-4 --classifiers 2-3-4-5-6-7 --batch-size 96 --seed 0 --drop-path-prob 0.2 --data cifar10 --cutout --gpu 1 --eval anytime

```