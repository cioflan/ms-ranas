#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    arguments.py                                                      #
# Author:  Cristian Cioflan, adapted from Hanxiao Liu (arXiv:1806.09055)     #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          # 
#                                                                            #   
# The file allows the user to decide on the arguments with which the         #
# searching or training process will be run. The arguments are split in four #
# groups: environment variables, data variables (i.e. dataset selection and  #
# inclusion of preprocessing techniques), architecture variables (i.e.       #
# selecting the genotype and the additional structures which will be added), #
# and training variables (i.e. training and searching hyperparameters).      #
#----------------------------------------------------------------------------#


import os
import glob
import time
import argparse

available_models = ['S_NU_S124_2345CS_CUT_2N']

args_parser = argparse.ArgumentParser(description = 'Image classification script')

environment_group = args_parser.add_argument_group('environment_group', 'environment setup')
environment_group.add_argument('--save', default = 'save/{}'.format('_other_variables'), type = str)
environment_group.add_argument('--resume', default = 'save/{}'.format('_other_variables'), action = 'store_true')
environment_group.add_argument('--load', default = 'save/{}'.format('_other_variables'), type = str)
environment_group.add_argument('--eval', default = 'none', type = str, choices = ['anytime', 'dynamic', 'none'])
environment_group.add_argument('--resources', default = 800000, type = int, help = 'Available resources in dynamic mode')
environment_group.add_argument('--seed', default = 42, type = int)  # training - 0; searching - 2
environment_group.add_argument('--gpu', default = None, type = str)
environment_group.add_argument('--search', default = False, type = bool)
environment_group.add_argument('--evaluate-from', default=None, type=str, help='path to saved checkpoint')
environment_group.add_argument('--quantize', action = 'store_true', default = False)

data_group = args_parser.add_argument_group('data_group', 'dataset setup')
data_group.add_argument('--data', default = 'cifar10')
data_group.add_argument('--data-path', default='data-path', type = str)
data_group.add_argument('--use-valid', action='store_true', help='use validation set or not')
data_group.add_argument('-j', '--workers', default=4, type=int, help='number of data loading workers (default: 4)')
data_group.add_argument('--cutout', action='store_true', default=False, help='use cutout')
data_group.add_argument('--cutout_length', type=int, default=16, help='cutout length')

architecture_group = args_parser.add_argument_group('architecture_group', 'architecture setup')
architecture_group.add_argument('--arch', default = 'S_NU_Scale124_2345Class', type = str, choices = available_models)
architecture_group.add_argument('--lossmode', default = 1, type = int)  # 1 - acc; 2 - acc + flops; 3 - acc + params; 4 - acc + flops + params
architecture_group.add_argument('--memweight', default = 1, type = float)
architecture_group.add_argument('--latencyweight', default = 1, type = float)
architecture_group.add_argument('--reduction', default = 0.5, type = float)
architecture_group.add_argument('--nClassifiers', type = int, default = 1)
architecture_group.add_argument('--classifiers', default='2-3-4-5', type = str)  # always have a classifier on the last layer
architecture_group.add_argument('--nBlocks', type = int, default = 5)
architecture_group.add_argument('--nChannels', type = int, default = 16)
architecture_group.add_argument('--base', type = int, default = 4)  # layers in block 0
architecture_group.add_argument('--stepmode', type = str, default = 'unit', choices = ['unit', 'linear_grow'])
architecture_group.add_argument('--step', type=int, default=1)  # layer increase per block
architecture_group.add_argument('--growthRate', type = int, default = 6)
# number of elements == number of scales
# value of elements  == multiplicaton in number of output channels 
architecture_group.add_argument('--growthFactor', default='1-2-4', type = str)
architecture_group.add_argument('--prune', default = 'max', choices = ['min', 'max'], type = str)
architecture_group.add_argument('--drop-path-prob', default = 0.3, type = float)  # training - 0.2; searching - 0.3
architecture_group.add_argument('--unrolled', action='store_true', default=False, help='use one-step unrolled validation loss')
architecture_group.add_argument('--grad_clip', type=float, default=5, help='gradient clipping')
architecture_group.add_argument('--intermediate-nodes', type=int, default=4)

training_group = args_parser.add_argument_group('training_group','training setup')
training_group.add_argument('--epochs', default = 50, type = int)
training_group.add_argument('--start-epoch', default = 0, type = int)
training_group.add_argument('--batch-size', default = 64, type = int)  # training - 96; searching - 64
training_group.add_argument('--optimizer', default = 'sgd', choices = ['sgd', 'rmsprop', 'adam'])
training_group.add_argument('--learning-rate', default = 0.025, type = float)
training_group.add_argument('--learning-rate-min', type=float, default=0.001, help='min learning rate')
training_group.add_argument('--lr-type', default = 'multistep', type = str, choices = ['cosine', 'multistep'])
training_group.add_argument('--decay-rate', default = 0.1, type = float)
training_group.add_argument('--momentum', default = 0.9, type = float)
training_group.add_argument('--weight-decay', default = 3e-4, type = float)
training_group.add_argument('--print-freq', default = 50, type = int)

