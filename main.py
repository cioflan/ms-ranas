#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    main.py                                                           #
# Author:  Cristian Cioflan                                                  #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          #
#                                                                            #
# The main() function initializes the environment variables, instantiates    #
# the auxiliary objects required for the training and/or searching process,  #
# then initiates and controls that process. The process is logged and its    #
# results are stored.                                                        #
#----------------------------------------------------------------------------#


import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.nn.functional as F
import os
import time
import numpy as np
import utils as utils
import genotypes
import visualise as visualise
import shutil
import torch.quantization

from architect import Architect
from arguments import args_parser
from data_loader import data_load
from model import Model as Network
from model_generator import Model as NetworkGen
from utils import AverageMeter
from utils import Tester
from operation_counter import measure_model


def main():

    args = args_parser.parse_args()
    if args.gpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    torch.cuda.set_device(0)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)
    np.random.seed(args.seed)
    cudnn.benchmark = True
    cudnn.enabled = True

    args.growthFactor = list(map(int, args.growthFactor.split('-')))
    args.nScales = len(args.growthFactor)
    args.classifiers = list(map(int, args.classifiers.split('-')))
    
    # load the data / download it if no path was specified / throw error is data is not downloadable
    if not os.path.exists(args.save):
        os.makedirs(args.save)

    if args.use_valid:
        args.splits = ['train', 'val', 'test']
    else:
        args.splits = ['train', 'val']

    if args.data == 'cifar10':
        args.num_classes = 10
    elif args.data == 'cifar100':
        args.num_classes = 100
    elif args.data == 'imagenet':
        args.num_classes = 1000
    else:
        print ("Unknown dataset | num classes")    

    if args.data.startswith('cifar'):
        IMG_SIZE = 32
    else:
        IMG_SIZE = 224

    genotype = eval("genotypes.%s" % args.arch)

    # printing arguments for logging purposes
    print (args)
    train_loader, val_loader, test_loader = data_load(args)
    num_train = len(train_loader)
    indices = list(range(num_train))
    train_portion = 0.5
    split = int(np.floor(train_portion * num_train))
    criterion = nn.CrossEntropyLoss().cuda()
    best_prec1, best_epoch = 0.0, 0

    if (args.search):
        model = NetworkGen(args)
        model = (model).cuda()
        if (args.start_epoch != 0):
            utils.load(model, args.evaluate_from)
        architect = Architect(model, args)
        optimizer = torch.optim.SGD (
        #weight_params,
        model.parameters(),
        args.learning_rate,
        momentum=args.momentum,
        weight_decay=args.weight_decay)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(args.epochs), eta_min = args.learning_rate_min)
    else:
        model = Network(args)
        model = (model).cuda()     	
        optimizer = torch.optim.SGD (
        #weight_params,
        model.parameters(),
        args.learning_rate,
        momentum=args.momentum,
        weight_decay=args.weight_decay)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(args.epochs))
        if (args.start_epoch != 0):            
            checkpoint = load_checkpoint(args)
            if checkpoint is not None:
                args.start_epoch = checkpoint['epoch'] + 1
                best_prec1 = checkpoint['best_prec1']
                model.load_state_dict(checkpoint['state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer']),
                scheduler.load_state_dict(checkpoint['scheduler'])
            utils.load(model, os.path.join(args.evaluate_from, 'weights.pt'))

    network_size = utils.count_parameters_in_MB(model)
    print ("Network size: " + str(network_size))
    n_flops, n_params = measure_model(model, IMG_SIZE, IMG_SIZE)    
    torch.save(n_flops, os.path.join(args.save, 'flops.pth'))
       
    scores = ['epoch\tlr\ttrain_loss\tval_loss\ttrain_prec1'
          '\tval_prec1\ttrain_prec5\tval_prec5']

    mode = 'normal'
    if (args.eval == 'anytime'):
        checkpoint = load_checkpoint(args)
        if checkpoint is not None:
            model.load_state_dict(checkpoint['state_dict'])
        utils.load(model, os.path.join(args.evaluate_from, 'weights.pt'))
        validate(test_loader, model, criterion, args, mode)
        return
    elif (args.eval == 'dynamic'):
        utils.load(model, args.evaluate_from)
        dynamic_validate(test_loader, val_loader, model, args)
        return
    else:
        # proceed with training
        print ("***** Training started *****")

    lr = None
    if(args.quantize):
            print ("***** Quantization started HERE *****")
            cpu_model_dict = {}
            cpu_model = Network(args)
            checkpoint = load_checkpoint(args)
            if checkpoint is not None:
                args.start_epoch = checkpoint['epoch'] + 1
                best_prec1 = checkpoint['best_prec1']
                model.load_state_dict(checkpoint['state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer']),
                scheduler.load_state_dict(checkpoint['scheduler'])
            model_dict = model.state_dict()
            for key, val in model_dict.items():
                cpu_model_dict[key] = val.cpu()
            cpu_model.load_state_dict(cpu_model_dict)
            quantized_model = torch.quantization.quantize_dynamic(cpu_model, {nn.ReLU, nn.Conv2d, nn.BatchNorm2d, nn.Linear}, dtype=torch.qint8)
            mode = 'quantize'
            val_loss, val_prec1, val_prec5 = validate(val_loader, quantized_model, criterion, args, mode)
            utils.save(quantized_model, os.path.join(args.evaluate_from, 'weights_quantized.pt'))
            print ("***** Quantization ended HERE *****") 
            return

    if (args.search):
        for epoch in range (args.start_epoch, args.epochs):
            scheduler.step()
            is_best = True
            scores = None
            lr = scheduler.get_lr()
            genotype = model.genotype()
            # print net's structure
            print (genotype)
            train_acc, train_obj = train_gen(train_loader, val_loader, model, architect, criterion, optimizer, lr, args, epoch)
            valid_acc, valid_obj = validate_gen(val_loader, model, criterion, args, epoch)
            utils.save(model, os.path.join(args.save, 'weights_search.pt'))
    else:
        for epoch in range (args.start_epoch, args.epochs):
            scheduler.step()
            lr = float(scheduler.get_lr()[0])
            train_loss, train_prec1, train_prec5, _ = train(train_loader, model, criterion, optimizer, epoch, args)
            val_loss, val_prec1, val_prec5 = validate(val_loader, model, criterion, args, mode)
            scores.append(('{}\t{:.3f}' + '\t{:.4f}' * 6)
                          .format(epoch, lr, train_loss, val_loss,
                                  train_prec1, val_prec1, train_prec5, val_prec5))

            is_best = val_prec1 > best_prec1
            if is_best:
                state = {
                    'epoch': epoch,
                    'state_dict': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    'scheduler': scheduler.state_dict(),                    
                }
                torch.save(state, os.path.join(args.save, 'state.pt'))
                utils.save(model, os.path.join(args.save, 'weights.pt'))
                best_prec1 = val_prec1
                best_epoch = epoch
                print('Best var_prec1 {}'.format(best_prec1))
            model_filename = str(os.path.join(args.save)) + ('_%03d.pth.tar' % epoch)
            save_checkpoint({
                'epoch': epoch,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_prec1': best_prec1,
                'optimizer': optimizer.state_dict(),
                'scheduler': scheduler.state_dict()
            }, args, is_best, model_filename, scores)

        print ("***** Training finished *****")
        if(args.quantize):
            print ("***** Quantization started *****")
            cpu_model_dict = {}
            cpu_model = Network(args)
            model_dict = model.state_dict()
            for key, val in model_dict.items():
                cpu_model_dict[key] = val.cpu()
            cpu_model.load_state_dict(cpu_model_dict)
            quantized_model = torch.quantization.quantize_dynamic(cpu_model, {nn.ReLU, nn.Conv2d, nn.BatchNorm2d, nn.Linear}, dtype=torch.qint16)
            mode = 'quantize'
            val_loss, val_prec1, val_prec5 = validate(val_loader, quantized_model, criterion, args, mode)  

    return

def train(train_loader, model, criterion, optimizer, epoch, args):

    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1, top5 = [], []

    for i in range(len(model.classifier)):
        top1.append(AverageMeter())
        top5.append(AverageMeter())

    model.train()
    end = time.time()
    running_lr = None

    for i, (input, target) in enumerate(train_loader):
        lr = args.learning_rate
        if running_lr is None:
            running_lr = lr
        data_time.update(time.time() - end)
        input = torch.autograd.Variable(input).cuda()
        target = torch.autograd.Variable(target).cuda()
        input_var = torch.autograd.Variable(input).cuda()
        target_var = torch.autograd.Variable(target).cuda()
        optimizer.zero_grad()
        output = model(input_var)
        if not isinstance(output, list):
            output = [output]

        loss = 0.0
        for j in range(len(output)):
            loss += criterion(output[j], target_var)
        losses.update(loss.item(), input.size(0))

        for j in range(len(output)):
            prec1, prec5 = utils.accuracy(output[j].data, target, topk=(1, 5))
            top1[j].update(prec1.item(), input.size(0))
            top5[j].update(prec5.item(), input.size(0))

        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                    'Time {batch_time.avg:.3f}\t'
                    'Data {data_time.avg:.3f}\t'
                    'Loss {loss.val:.4f}\t'
                    'Acc@1 {top1.val:.4f}\t'
                    'Acc@5 {top5.val:.4f}'.format(
                        epoch, i + 1, len(train_loader),
                        batch_time=batch_time, data_time=data_time,
                        loss=losses, top1=top1[-1], top5=top5[-1]))

    return losses.avg, top1[-1].avg, top5[-1].avg, running_lr

def validate(val_loader, model, criterion, args, mode):
    batch_time = AverageMeter()
    losses = AverageMeter()
    data_time = AverageMeter()
    top1, top5 = [], []

    for i in range(len(model.classifier)):
        top1.append(AverageMeter())
        top5.append(AverageMeter())

    model.eval()
    end = time.time()
    with torch.no_grad():
        for i, (input, target) in enumerate(val_loader):

            if (mode == 'quantize'):
                target = target
                # target = target.cuda(async=True)
                input = input
                input_var = torch.autograd.Variable(input)
                target_var = torch.autograd.Variable(target)
            else:
                target = target.cuda()
                # target = target.cuda(async=True)
                input = input.cuda()
                input_var = torch.autograd.Variable(input).cuda()
                target_var = torch.autograd.Variable(target).cuda()

            data_time.update(time.time() - end)
            output = model(input_var)
            if not isinstance(output, list):
                output = [output]

            loss = 0.0
            for j in range(len(output)):
                loss += criterion(output[j], target_var)
            losses.update(loss.item(), input.size(0))

            for j in range(len(output)):
                prec1, prec5 = utils.accuracy(output[j].data, target, topk=(1, 5))
                top1[j].update(prec1.item(), input.size(0))
                top5[j].update(prec5.item(), input.size(0))

            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                    print('Epoch: [{0}/{1}]\t'
                    'Time {batch_time.avg:.3f}\t'
                    'Data {data_time.avg:.3f}\t'
                    'Loss {loss.val:.4f}\t'
                    'Acc@1 {top1.val:.4f}\t'
                    'Acc@5 {top5.val:.4f}'.format(
                        i + 1, len(val_loader),
                        batch_time=batch_time, data_time=data_time,
                        loss=losses, top1=top1[-1], top5=top5[-1]))
        
    # visualise.confusion_matrix(val_loader, model, args)
    for j in range(len(model.classifier)):
        print(' * prec@1 {top1.avg:.3f} prec@5 {top5.avg:.3f}'.format(top1=top1[j], top5=top5[j]))
    
    return losses.avg, top1[-1].avg, top5[-1].avg

def train_gen (train_queue, valid_queue, model, architect, criterion, optimizer, lr, args, epoch):
    objs = AverageMeter()  # this defines the loss
    batch_time = AverageMeter()
    data_time = AverageMeter()
    top1, top5 = [], []

    for i in range(len(model.classifier)):
        top1.append(AverageMeter())
        top5.append(AverageMeter())
    end = time.time()

    for step, (input, target) in enumerate(train_queue):
        model.train()
        n = input.size(0)
        input = torch.autograd.Variable(input, requires_grad=False).cuda()
        target = torch.autograd.Variable(target, requires_grad=False).cuda()
        input_search, target_search = next(iter(valid_queue))
        input_search = torch.autograd.Variable(input_search, requires_grad=False).cuda()
        target_search = torch.autograd.Variable(target_search, requires_grad=False).cuda()
        data_time.update(time.time() - end)
        
        architect.step(input, target, input_search, target_search, lr, optimizer, unrolled=args.unrolled)
        optimizer.zero_grad()
        output = model(input)
        loss = 0.0
        for i in range (len(output)):
            loss += criterion(output[i], target)
        loss.backward()
        nn.utils.clip_grad_norm(model.parameters(), args.grad_clip)
        optimizer.step()

        for j in range(len(output)):
            prec1, prec5 = utils.accuracy(output[j].data, target, topk=(1, 5))
            top1[j].update(prec1.item(), input.size(0))
            top5[j].update(prec5.item(), input.size(0))
        objs.update(loss.data, n)
        if step % args.print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                    'Time {batch_time.avg:.3f}\t'
                    'Data {data_time.avg:.3f}\t'
                    'Loss {loss.val:.4f}\t'
                    'Acc@1 {top1.val:.4f}\t'
                    'Acc@5 {top5.val:.4f}'.format(
                        epoch, step + 1, len(train_queue),
                        batch_time=batch_time, data_time=data_time,
                        loss=objs, top1=top1[-1], top5=top5[-1]))
    
    return top1[-1].avg, objs.avg

def validate_gen(valid_queue, model, criterion, args, epoch):
    objs = AverageMeter()  # this defines the loss
    batch_time = AverageMeter()
    data_time = AverageMeter()
    top1, top5 = [], []

    for i in range(len(model.classifier)):
        top1.append(AverageMeter())
        top5.append(AverageMeter())

    model.eval()
    end = time.time()
    with torch.no_grad():
        for step, (input, target) in enumerate(valid_queue):
            input = torch.autograd.Variable(input).cuda()
            target = torch.autograd.Variable(target).cuda()
            data_time.update(time.time() - end)
            output = model(input)
            loss = 0.0
            for j in range(len(output)):
                loss += criterion(output[j], target)
            for j in range(len(output)):
                prec1, prec5 = utils.accuracy(output[j].data, target, topk=(1, 5))
                top1[j].update(prec1.item(), input.size(0))
                top5[j].update(prec5.item(), input.size(0))
            n = input.size(0)
            objs.update(loss.data, n)
            if step % args.print_freq == 0:
                print('Epoch: [{0}][{1}/{2}]\t'
                        'Time {batch_time.avg:.3f}\t'
                        'Data {data_time.avg:.3f}\t'
                        'Loss {loss.val:.4f}\t'
                        'Acc@1 {top1.val:.4f}\t'
                        'Acc@5 {top5.val:.4f}'.format(
                            epoch, step + 1, len(valid_queue),
                            batch_time=batch_time, data_time=data_time,
                            loss=objs, top1=top1[-1], top5=top5[-1]))

    for j in range(len(model.classifier)):
        print(' * prec@1 {top1.avg:.3f} prec@5 {top5.avg:.3f}'.format(top1=top1[j], top5=top5[j]))

    return top1[-1].avg, objs.avg

def dynamic_validate(test_loader, val_loader, model, args):
    tester = Tester(model, args)
    val_pred, val_target = tester.calc_logit(val_loader) 
    test_pred, test_target = tester.calc_logit(test_loader) 
    torch.save((val_pred, val_target, test_pred, test_target), 
                   os.path.join(args.save, 'logits_single.pth'))    
    flops = torch.load(os.path.join(args.save, 'flops.pth'))
    with open(os.path.join(args.save, 'dynamic.txt'), 'w') as fout:
        for p in range(1, 40):
            print("*********************")
            _p = torch.FloatTensor(1).fill_(p * 1.0 / 20)
            probs = torch.exp(torch.log(_p) * torch.range(1, len(model.classifier)))
            probs /= probs.sum()
            acc_val, _, T = tester.dynamic_eval_find_threshold(
                val_pred, val_target, probs, flops)
            acc_test, exp_flops = tester.dynamic_eval_with_threshold(
                test_pred, test_target, flops, T)
            print('valid acc: {:.3f}, test acc: {:.3f}, test flops: {:.2f}M'.format(acc_val, acc_test, exp_flops / 1e6))
            fout.write('{}\t{}\n'.format(acc_test, exp_flops.item()))

def save_checkpoint(state, args, is_best, filename, result):
    print(args)
    result_filename = os.path.join(args.save, 'scores.tsv')
    model_dir = os.path.join(args.save, 'save_models')
    latest_filename = os.path.join(model_dir, 'latest.txt')
    model_filename = os.path.join(model_dir, filename)
    best_filename = os.path.join(model_dir, 'model_best.pth.tar')
    os.makedirs(args.save, exist_ok=True)
    os.makedirs(model_dir, exist_ok=True)
    print("=> saving checkpoint '{}'".format(model_filename))

    torch.save(state, model_filename)
    with open(result_filename, 'w') as f:
        print('\n'.join(result), file=f)

    with open(latest_filename, 'w') as fout:
        fout.write(model_filename)
    if is_best:
        shutil.copyfile(model_filename, best_filename)
    print("=> saved checkpoint '{}'".format(model_filename))

    return

def load_checkpoint(args):
    print ("Loading checkpoint")
    model_dir = os.path.join(args.evaluate_from, 'save_models')
    latest_filename = os.path.join(model_dir, 'latest.txt')
    if os.path.exists(latest_filename):
        with open(latest_filename, 'r') as fin:
            model_filename = fin.readlines()[0].strip()
    else:
        print ("None")
        return None
    print("=> loading checkpoint '{}'".format(model_filename))
    state = torch.load(model_filename)
    print("=> loaded checkpoint '{}'".format(model_filename))

    return state

if __name__ == '__main__':
    main()


