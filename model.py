#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    model.py                                                          #
# Author:  Cristian Cioflan                                                  #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          #
#                                                                            #
# The Network describes the structure of the network to be trained and/or    # 
# evaluated, considering the layers that are included, the Cells and the     #
# way they interact to determine the dataflow. A Cell is a structure found   #
# using neural architecture search, comprised of neural network operations   #
# interacting in an optimal way for a specified task.                        #
#----------------------------------------------------------------------------#


import torch.nn as nn
import torch
import math
import genotypes

from torch.autograd import Variable
from operations import *
from utils import drop_path


class Model (nn.Module):
    def __init__ (self, args):
        super(Model, self).__init__()
        self.args = args
        self.n_scales = len(args.growthFactor)
        self.scale_array = args.growthFactor
        self.n_classifiers = len (args.classifiers)
        self.pos_classifiers = args.classifiers

        self.inChannels = args.nChannels
        self.nBlocks = args.nBlocks
        self.drop_path_prob = args.drop_path_prob        

        self.n_layers_total = args.nBlocks
        n_layers_total = self.n_layers_total

        # Channels:
        # 1) Cpp = # Channels of previous-previous cell
        # 2) Cp  = # Channels of previous cell
        # 3) Cc  = # Channels of current cell

        stem_multiplier = 3
        cc = stem_multiplier * self.inChannels
        self.stem = nn.Sequential(
            nn.Conv2d(3, cc, 3, padding=1, bias=False),
            nn.BatchNorm2d(cc)
            )
        self.stem_array = []
        self.stem_array.append(self.stem)

        for i in range(1, self.n_scales):
            # i controls the feature map size
            # self.scale_array[i] controls the number of channels for each feature map
            self.temp_stem = nn.Sequential(
                nn.Conv2d(3, cc, 3, stride=1, padding=0, bias=False),
                # padding = 2, as sizes must be even
                nn.Conv2d(cc, self.scale_array[i] * cc, 3, stride=i*2, padding=2, bias=False),
                nn.BatchNorm2d(self.scale_array[i] * cc)
                )
            self.stem_array.append(
                self.temp_stem.cuda()
            )

        cpp, cp, cc = cc, cc, self.inChannels        
        self.cells = nn.ModuleList()
        self.classifier = nn.ModuleList()

        reduction_prev = False
        downsample = None
        conv_prev = False
        addition = 0
        local_scales = self.n_scales
        local_scale_array = self.scale_array

        for i in range (n_layers_total):
            # channel reduction at each third of the net's depth
            if i in [n_layers_total//3, 2*n_layers_total//3]:
                cc = cc * 2
                reduction = True
            else:
                reduction = False
                
            # Intermediate layers classifier
            for position in self.pos_classifiers:
                if ((i == position - 1) and (i != n_layers_total - 1) and (i != 0)):
                    if (args.data.startswith('cifar') or args.data.startswith('imagenet')):
                        self.classifier.append(ClassifierV1(self.cells[-1], local_scale_array[-1] * cell.multiplier * cc , args.num_classes))   
                    else:
                        print ("Unknown dataset")    
            genotype = eval("genotypes.%s" % args.arch)

            # 1 DownConv previous cell
            # 2 concat (1) with current cell
            # 3 append (2)

            for j in range (0, local_scales):
                passed_scale = local_scale_array[j]
                cell = Cell (genotype, local_scale_array[j] * cpp, local_scale_array[j] * cp, local_scale_array[j] * cc, reduction, reduction_prev, conv_prev, passed_scale)      

                conv_prev = False                               
                self.cells += [cell]

                # First layer classifier
                if (j == local_scales - 1 and i == 0 and self.pos_classifiers[0] == 1):
                   if (args.data.startswith('cifar') or args.data.startswith('imagenet')):
                       self.classifier.append(ClassifierV1(self.cells[-1], local_scale_array[-1] * cell.multiplier * cc , args.num_classes))   
                   else:
                       print ("Unknown dataset")

                if (j == local_scales - 1):
                    continue
                else:
                    conv_prev = True
                    addition += local_scale_array[j] * cp
                    downsample = ConvDownNormal(local_scale_array[j] * cell.multiplier * cc, local_scale_array[j+1] * cpp, 
                        local_scale_array[j+1] * cpp, reduction, reduction_prev) 
                    self.cells += [downsample]
                    
            reduction_prev = reduction
            cpp, cp = cp, cell.multiplier * cc
            if i == 2 * n_layers_total//3:
                c_to_aux = cp        

        # Last layer classifier (mandatory)
        if (args.data.startswith('cifar') or args.data.startswith('imagenet')):
            self.classifier.append(
                    ClassifierV1(self.cells[-1], local_scale_array[-1] * cp, args.num_classes)
                )
        else:
            print ("Unknown dataset")

    def forward(self, input):
        logits = []
        classifier_parser = 0

        s0 = [None] *  self.n_scales
        s1 = [None] *  self.n_scales
        downsamples = [None] * (self.n_scales - 1)
        set_parser = 0

        # s0[0]: largest scale
        # so[n_scales - 1]: smallest scale
        for i in range(0, self.n_scales):
            s0[i] = self.stem_array[i](input)
            s1[i] = self.stem_array[i](input) 

        current_scale = 0  # in range 0 - number of scales
        small_cell = None  # placeholder for the last cell that will be passed to the classifier
        prev_cell = False  # True if previous action was a MS-RANAS cell computation
        prev_conv = False  # True if previous action was a DownConv computation
        comming_layer = 0
        local_scales = self.n_scales

        # reduction layers
        r1 = self.n_layers_total//3
        r2 = 2*self.n_layers_total//3

        for i, cell in enumerate (self.cells):
            if(local_scales == 1):
                s0[0], s1[0] = s1[0], cell(s0[0], s1[0], self.drop_path_prob)
                small_cell = s1[0]
                comming_layer = comming_layer + 1                
            else:
                if (current_scale == 0):  # largest scale
                    s0[current_scale], s1[current_scale] = s1[current_scale], cell(s0[current_scale], s1[current_scale], self.drop_path_prob)
                    current_scale += 1
                    prev_cell = True
                    prev_conv = False                    
                else:    
                    if (prev_cell):
                        downsamples[current_scale-1] = cell(s1[current_scale-1], s0[current_scale])
                        prev_cell = False
                        prev_conv = True                        
                    else:                          
                        s0[current_scale], s1[current_scale] = s1[current_scale], cell(downsamples[current_scale-1], s1[current_scale], self.drop_path_prob) 
                        current_scale += 1
                        prev_cell = True
                        # smallest cell is used for classification
                        if (current_scale == local_scales):
                            small_cell = s1[current_scale-1]
                            current_scale = 0
                            comming_layer = comming_layer + 1
                        prev_conv = False         
            
            for position in self.pos_classifiers:
                if (current_scale == 0 and position == comming_layer):
                    logits.append(self.classifier[classifier_parser](small_cell))
                    classifier_parser += 1

        return logits


class Cell (nn.Module):
    def __init__ (self, genotype, cpp, cp, cc, reduction, reduction_prev, conv_prev, current_scale):
        super(Cell, self).__init__()
        self.reduction = reduction
        self.current_scale  = current_scale 
        self.conv_prev = conv_prev
        if (conv_prev):
            if (reduction == False):
                if (reduction_prev == True):
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 2, 0)
                else:
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)
            else:
                if (reduction_prev == True):
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)
                else:
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)
        else:
            if (reduction_prev):
                self.preprocess0 = FactorizedReduce(cpp, cc)
            else:
                self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0) 
        self.preprocess1 = ReLUConvBN(cp, cc, 1, 1, 0)

        if reduction:
            op_names, indices = zip(*genotype.reduce)
            concat = genotype.reduce_concat
        else:
            op_names, indices = zip(*genotype.normal)
            concat = genotype.normal_concat
        self._compile(cc, op_names, indices, concat, reduction)

    def _compile (self, cc, op_names, indices, concat, reduction):
        assert len(op_names) == len(indices)
        self._steps = len(op_names)//2
        self._concat = concat
        self.multiplier = len(concat)
        self._ops = nn.ModuleList()
        for name, index in zip(op_names, indices):
            if reduction and index < 2:
                stride = 2
            else:
                stride = 1
            op = OPS[name](cc, stride, True)
            self._ops += [op]
        self._indices = indices

    def forward (self, s0, s1, drop_prob):
        s1_init = s1
        s0 = self.preprocess0(s0)
        s1 = self.preprocess1(s1)
        states = [s0, s1]

        for i in range (self._steps):
            h1 = states[self._indices[2*i]]
            h2 = states[self._indices[2*i+1]]   

            op1 = self._ops[2*i]
            op2 = self._ops[2*i+1]

            h1 = op1(h1)
            h2 = op2(h2)

            if self.training and drop_prob > 0.:
                if not isinstance(op1, Identity):
                    h1 = drop_path(h1, drop_prob)
                if not isinstance(op2, Identity):
                    h2 = drop_path(h2, drop_prob)                

            s = h1 + h2
            states += [s]           

        res = []
        for i in self._concat:
            res.append(states[i])

        return torch.cat(res, dim = 1)















				
