#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    model_generator.py                                                #
# Author:  Cristian Cioflan                                                  #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          #
#                                                                            #
# The Network describes the structure of the network within which a Cell     #
# that is searched for will be integrated, considering the layers that are   #
# included, the Cells and the way they interact to determine the dataflow.   #
# A Cell is a structure found using neural architecture search, comprised of #
# neural network operations from a predefined set and interacting in an      #
# optimal way (i.e. achieving a low-value loss) for a specified task.        #
#----------------------------------------------------------------------------#


import torch.nn as nn
import torch
import math
import torch.nn.functional as F
import genotypes
import utils
import operation_counter
import numpy as np

from torch.autograd import Variable
from operations import *
from utils import drop_path
from genotypes import PRIMITIVES
from genotypes import Genotype


class MixedOp(nn.Module):
  def __init__(self, cc, stride):
    super(MixedOp, self).__init__()
    self._ops = nn.ModuleList()
    for primitive in PRIMITIVES:
        op = OPS[primitive](cc, stride, False)
        if 'pool' in primitive:
          op = nn.Sequential(op, nn.BatchNorm2d(cc, affine=False))
        self._ops.append(op)

  def forward(self, x, weights):    
    return sum(w * op(x) for w, op in zip(weights, self._ops))


class Model (nn.Module):
    def __init__ (self, args):
        super(Model, self).__init__()
        self.args = args
        self.n_scales = len(args.growthFactor)
        self.scale_array = args.growthFactor        

        self.inChannels = args.nChannels
        self.nBlocks = args.nBlocks
        self.drop_path_prob = args.drop_path_prob
        self.criterion = nn.CrossEntropyLoss().cuda()
        self.classifier = nn.ModuleList()

        self.n_layers_total = args.nBlocks
        n_layers_total = self.n_layers_total
        self.steps = args.intermediate_nodes
        self.multiplier = args.intermediate_nodes

        self.n_classifiers = len (args.classifiers)
        self.pos_classifiers = args.classifiers

        # Channels:
        # 1) Cpp = # Channels of previous-previous cell
        # 2) Cp  = # Channels of previous cell
        # 3) Cc  = # Channels of current cell

        stem_multiplier = 3
        cc = stem_multiplier * self.inChannels
        self.stem = nn.Sequential(
            nn.Conv2d(3, cc, 3, padding=1, bias=False),
            nn.BatchNorm2d(cc)
            )
        self.stem_array = []
        self.stem_array.append(self.stem)

        for i in range(1, self.n_scales):
            # i controls the feature map size
            # self.scale_array[i] controls the number of channels for each feature map
            self.temp_stem = nn.Sequential(
                nn.Conv2d(3, cc, 3, stride=1, padding=0, bias=False),
                # padding = 2, as sizes must be even
                nn.Conv2d(cc, self.scale_array[i] * cc, 3, stride=i*2, padding=2, bias=False),
                nn.BatchNorm2d(self.scale_array[i] * cc)
                )
            self.stem_array.append(
                self.temp_stem.cuda()
            )

        cpp, cp, cc = cc, cc, self.inChannels        
        self.cells = nn.ModuleList()
        reduction_prev = False
        downsample = None
        conv_prev = False

        for i in range (n_layers_total):
            # channel reduction at each third of the net's depth
            if i in [n_layers_total//3, 2*n_layers_total//3]:
                cc = cc * 2
                reduction = True                
            else:
                reduction = False

            # Intermediate layers classifier
            for position in self.pos_classifiers:
                if ((i == position - 1) and (i != n_layers_total - 1) and (i != 0)):
                    if (args.data.startswith('cifar') or args.data.startswith('imagenet')):
                        self.classifier.append(ClassifierV1(self.cells[-1], self.scale_array[-1] * cell.multiplier * cc , args.num_classes))   
                    else:
                        print ("Unknown dataset") 

            genotype = eval("genotypes.%s" % args.arch)

            for j in range (0, self.n_scales):
                passed_scale = self.scale_array[j]
                cell = Cell (self.n_scales, self.steps, self.multiplier, self.scale_array[j] * cpp, 
                    self.scale_array[j] * cp, self.scale_array[j] * cc, reduction, reduction_prev, conv_prev, passed_scale)    
                conv_prev = False                               
                self.cells += [cell]            

                # First layer classifier
                if (j == self.n_scales - 1 and i == 0 and self.pos_classifiers[0] == 1):
                   if (args.data.startswith('cifar') or args.data.startswith('imagenet')):
                       self.classifier.append(ClassifierV1(self.cells[-1], self.scale_array[-1] * cell.multiplier * cc , args.num_classes))   
                   else:
                       print ("Unknown dataset")
                        
                if (j == self.n_scales - 1):
                    continue
                else:
                    conv_prev = True
                    downsample = ConvDownNormal(self.scale_array[j] * self.multiplier * cc, self.scale_array[j+1] * cpp, 
                        self.scale_array[j+1] * cpp, reduction, reduction_prev)    
                    self.cells += [downsample]
            reduction_prev = reduction

            cpp, cp = cp, self.multiplier * cc
            if i == 2 * n_layers_total//3:
                c_to_aux = cp

        # Last layer classifier (mandatory)
        if (args.data.startswith('cifar')  or args.data.startswith('imagenet')):
            self.classifier.append(
                    ClassifierV1(self.cells[-1], self.scale_array[-1] * self.multiplier * cc, args.num_classes)
                )
        else:
            print ("Unknown dataset")
        self._initialize_alphas() 

    def new(self):
        model_new = Model(self.args)
        for x, y in zip(model_new.arch_parameters(), self.arch_parameters()):
            x.data.copy_(y.data)

        return model_new

    def forward(self, input):
        logits = []
        classifier_parser = 0

        s0 = [None] * self.n_scales
        s1 = [None] * self.n_scales
        downsamples = [None] * (self.n_scales - 1)
        
        # s0[0]: largest scale
        # so[n_scales - 1]: smallest scale
        for i in range(0, self.n_scales):
            s0[i] = self.stem_array[i](input)
            s1[i] = self.stem_array[i](input)           

        current_scale = 0  # in range 0 - number of scales
        small_cell = None  # placeholder for the last cell that will be passed to the classifier
        prev_cell = False  # True if previous action was a MS-RANAS cell computation
        prev_conv = False  # True if previous action was a DownConv computation        
        comming_layer = 0
        for i, cell in enumerate (self.cells):
            if cell.reduction:
                weights = F.softmax(self.alphas_reduce, dim=-1)
            else:
                weights = F.softmax(self.alphas_normal, dim=-1)
            if(self.n_scales == 1):
                s0[0], s1[0] = s1[0], cell(s0[0], s1[0], weights)   
                small_cell = s1[0]
                comming_layer = comming_layer + 1                
            else:
                if (current_scale == 0):  # largest scale
                    s0[current_scale], s1[current_scale] = s1[current_scale], cell(s0[current_scale], s1[current_scale], weights)
                    current_scale += 1
                    prev_cell = True
                    prev_conv = False                    
                else:    
                    if (prev_cell):
                        downsamples[current_scale-1] = cell(s1[current_scale-1], s0[current_scale])
                        prev_cell = False
                        prev_conv = True                        
                    else:                          
                        s0[current_scale], s1[current_scale] = s1[current_scale], cell(downsamples[current_scale-1], s1[current_scale], weights) 
                        current_scale += 1
                        prev_cell = True

                        # smallest cell is used for classification
                        if (current_scale == self.n_scales):
                            small_cell = s1[current_scale-1]
                            current_scale = 0
                            comming_layer = comming_layer + 1
                        prev_conv = False         

            for position in self.pos_classifiers:
                if (current_scale == 0 and position == comming_layer):
                    logits.append(self.classifier[classifier_parser](small_cell))
                    classifier_parser += 1

        return logits

    def _loss(self, input, target):
        IMG_SIZE = 32               
        logits = self(input)
        loss = self.criterion(logits[0], target) 
        for i in range (1, len(logits)):
            loss += self.criterion(logits[i], target)
        if (self.args.lossmode == 1):
            loss = loss
        elif (self.args.lossmode == 2):
            flops, _ = operation_counter.measure_model(self, IMG_SIZE, IMG_SIZE)
            flops_count = flops[-1]/100.0
            loss = loss + flops_count
        elif (self.args.lossmode == 3):
            param_count = utils.count_parameters_in_MB(self)
            loss = loss + param_count
        elif (self.args.lossmode == 4):
            self.args.genotpye = self.genotype()
            flops, _ = operation_counter.measure_model(self, IMG_SIZE, IMG_SIZE)
            flops_count = flops[-1]/100.0
            param_count = utils.count_parameters_in_MB(self)
            loss =  loss + self.args.latencyweight * flops_count + self.args.memweight * param_count
        else:
            print ("Loss mode unknown")

        return loss

    def _initialize_alphas(self):        
        k = sum(1 for i in range(0, self.steps) for n in range(2+i))
        num_ops = len(PRIMITIVES)

        self.alphas_normal = Variable(1e-3*torch.randn(k, num_ops).cuda(), requires_grad=True)
        self.alphas_reduce = Variable(1e-3*torch.randn(k, num_ops).cuda(), requires_grad=True)
        self._arch_parameters = [
          self.alphas_normal,
          self.alphas_reduce,
        ]

    def arch_parameters(self):
        return self._arch_parameters

    def genotype(self):

        def _parse(weights):
            gene = []
            n = 2
            start = 0
            for i in range(0, self.steps):
                end = start + n
                W = weights[start:end].copy()
                edges = sorted(range(i + 2), key=lambda x: -max(W[x][k] for k in range(len(W[x])) if k != PRIMITIVES.index('none')))[:2]

                for j in edges:
                    k_best = None
                    for k in range(len(W[j])):
                        if k != PRIMITIVES.index('none'):
                            if k_best is None or W[j][k] > W[j][k_best]:
                                k_best = k
                    gene.append((PRIMITIVES[k_best], j))
                start = end
                n += 1

            return gene

        gene_normal = _parse(F.softmax(self.alphas_normal, dim=-1).data.cpu().numpy())
        gene_reduce = _parse(F.softmax(self.alphas_reduce, dim=-1).data.cpu().numpy())        
        concat = range(2+self.steps-self.multiplier, self.steps+2)
        genotype = Genotype(
          normal=gene_normal, normal_concat=concat,
          reduce=gene_reduce, reduce_concat=concat
        )

        return genotype


class Cell (nn.Module):
    def __init__ (self, n_scales, steps, multiplier, cpp, cp, cc, reduction, reduction_prev, conv_prev, current_scale):
        super(Cell, self).__init__()
        self.reduction = reduction
        self.steps = steps
        self.multiplier = multiplier
        self.n_scales = n_scales

        if (conv_prev):
            if (reduction == False):
                if (reduction_prev == True):
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 2, 0)
                else:
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)
            else:
                if (reduction_prev == True):
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)
                else:
                    self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)
        else:
            if (reduction_prev):
                self.preprocess0 = FactorizedReduce(cpp, cc)
            else:
                self.preprocess0 = ReLUConvBN(cpp, cc, 1, 1, 0)         

        self.preprocess1 = ReLUConvBN(cp, cc, 1, 1, 0)
        self._ops = nn.ModuleList()
        self._bns = nn.ModuleList()

        for i in range(0, self.steps):
            for j in range(2+i):
                stride = 2 if reduction and j < 2 else 1
                op = MixedOp(cc, stride)
                self._ops.append(op)

    def forward(self, s0, s1, weights):
        s0 = self.preprocess0(s0)
        s1 = self.preprocess1(s1)

        states = [s0, s1]
        offset = 0
        for i in range (0, self.steps):
            s = sum(self._ops[offset+j](h, weights[offset+j]) for j, h in enumerate(states))
            offset += len(states)
            states.append(s)

        return torch.cat(states[-self.multiplier:], dim=1)
