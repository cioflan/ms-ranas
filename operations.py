#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    operations.py                                                     #
# Author:  Cristian Cioflan                                                  #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          #
#                                                                            #
# The file defines the neural network operations that can form the graph     #
# of a searched cell and/or which can be used in the final network between   #
# any two cells or to exit the network.                                      #
#----------------------------------------------------------------------------#

import torch
import torch.nn as nn


OPS = {
    'none' : lambda C, stride, affine: Zero(stride),
    'avg_pool_3x3' : lambda C, stride, affine: nn.AvgPool2d(3, stride=stride, padding=1, count_include_pad=False),
    'max_pool_3x3' : lambda C, stride, affine: nn.MaxPool2d(3, stride=stride, padding=1),
    'skip_connect' : lambda C, stride, affine: Identity() if stride == 1 else FactorizedReduce(C, C, affine=affine),
    'sep_conv_3x3' : lambda C, stride, affine: SepConv(C, C, 3, stride, 1, affine=affine),
    'sep_conv_5x5' : lambda C, stride, affine: SepConv(C, C, 5, stride, 2, affine=affine),
    'sep_conv_7x7' : lambda C, stride, affine: SepConv(C, C, 7, stride, 3, affine=affine),
    'dil_conv_3x3' : lambda C, stride, affine: DilConv(C, C, 3, stride, 2, 2, affine=affine),
    'dil_conv_5x5' : lambda C, stride, affine: DilConv(C, C, 5, stride, 4, 2, affine=affine),
    'conv_7x1_1x7' : lambda C, stride, affine: nn.Sequential(
        nn.ReLU(inplace=False),
        nn.Conv2d(C, C, (1,7), stride=(1, stride), padding=(0, 3), bias=False),
        nn.Conv2d(C, C, (7,1), stride=(stride, 1), padding=(3, 0), bias=False),
        nn.BatchNorm2d(C, affine=affine)
        ),
}


class ReLUConvBN(nn.Module):
    def __init__(self, C_in, C_out, kernel_size, stride, padding, affine=True):
        super(ReLUConvBN, self).__init__()
        self.op = nn.Sequential(
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_out, kernel_size, stride=stride, padding=padding, bias=False),
            nn.BatchNorm2d(C_out, affine=affine)
        )

    def forward(self, x):
        return self.op(x)


class DilConv(nn.Module):
    # ReLU, followed by a dilation convolution, then standard convolution, then BN        
    def __init__(self, C_in, C_out, kernel_size, stride, padding, dilation, affine=True):
        super(DilConv, self).__init__()
        self.op = nn.Sequential(
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_in, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=C_in, bias=False),
            nn.Conv2d(C_in, C_out, kernel_size=1, padding=0, bias=False),
            nn.BatchNorm2d(C_out, affine=affine),
            )

    def forward(self, x):
        return self.op(x)


class SepConv(nn.Module):
    # Depthwise Separable Convolution
    def __init__(self, C_in, C_out, kernel_size, stride, padding, affine=True):
        super(SepConv, self).__init__()
        self.op = nn.Sequential(
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_in, kernel_size=kernel_size, stride=stride, padding=padding, groups=C_in, bias=False),
            nn.Conv2d(C_in, C_in, kernel_size=1, padding=0, bias=False),
            nn.BatchNorm2d(C_in, affine=affine),
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_in, kernel_size=kernel_size, stride=1, padding=padding, groups=C_in, bias=False),
            nn.Conv2d(C_in, C_out, kernel_size=1, padding=0, bias=False),
            nn.BatchNorm2d(C_out, affine=affine),
            )

    def forward(self, x):
        return self.op(x)


class Identity(nn.Module):
    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        return x


class Zero(nn.Module):
    def __init__(self, stride):
        super(Zero, self).__init__()
        self.stride = stride

    def forward(self, x):
        if self.stride == 1:
            return x.mul(0.)

        return x[:,:,::self.stride,::self.stride].mul(0.)


class FactorizedReduce(nn.Module):
    def __init__(self, C_in, C_out, affine=True):
        super(FactorizedReduce, self).__init__()
        assert C_out % 2 == 0
        self.relu = nn.ReLU(inplace=False)
        self.conv_1 = nn.Conv2d(C_in, C_out // 2, 1, stride=2, padding=0, bias=False)
        self.conv_2 = nn.Conv2d(C_in, C_out // 2, 1, stride=2, padding=0, bias=False) 
        self.bn = nn.BatchNorm2d(C_out, affine=affine)

    def forward(self, x):
        # for a cell, C_in == C_prev_prev and C_out = C_prev
        x = self.relu(x)
        out = torch.cat([self.conv_1(x), self.conv_2(x[:,:,1:,1:])], dim=1)
        out = self.bn(out)

        return out


class FactorizedReduceDownscale(nn.Module):
    def __init__(self, C_in, C_out, affine=True):
        super(FactorizedReduceDownscale, self).__init__()
        assert C_out % 2 == 0
        self.relu = nn.ReLU(inplace=False)
        self.conv_1 = nn.Conv2d(C_in, C_out // 2, 1, stride=1, padding=0, bias=False)
        self.conv_2 = nn.Conv2d(C_in, C_out // 2, 1, stride=1, padding=0, bias=False) 
        self.bn = nn.BatchNorm2d(C_out, affine=affine)

    def forward(self, x):
        # for a cell, C_in == C_prev_prev and C_out = C_prev
        x = self.relu(x)
        out = torch.cat([self.conv_1(x), self.conv_2(x[:,:,:,:])], dim=1)
        out = self.bn(out)

        return out


class ConvBasic(nn.Module):
    def __init__(self, nIn, nOut, type: str, reduction, reduction_prev, kernel=3, stride=1, padding=1):
        super(ConvBasic, self).__init__()
        layer = []
        nInner = nIn
        if (reduction_prev == True and reduction == True):
            stride_norm = 2 
            stride_down = 1 
        elif (reduction == True or reduction_prev == True):
            stride_norm = 1
            stride_down = 1                 
        else:
            stride_norm = 1 
            stride_down = 2 
        if (type == 'norm'):
            layer.append(nn.Conv2d(nInner, nOut, kernel_size=kernel, stride=stride_norm,
                                    padding=padding, bias=False))
            layer.append(nn.BatchNorm2d(nOut))
            layer.append(nn.ReLU(True))
        else:
            layer.append(nn.Conv2d(nInner, nOut, kernel_size=kernel, stride=stride_down,
                                        padding=padding, bias=False))
            layer.append(nn.BatchNorm2d(nOut))
            layer.append(nn.ReLU(True))
        self.net = nn.Sequential(*layer)

    def forward(self, x):
        return self.net(x)


class ClassifierModule(nn.Module):
    def __init__(self, m, channel, num_classes):
        super(ClassifierModule, self).__init__()
        self.m = m
        self.linear = nn.Linear(channel, num_classes)

    def forward(self, x):
        res = self.m(x[-1])
        res = res.view(res.size(0), -1)

        return self.linear(x)


class ConvDownNormal(nn.Module):
    def __init__(self, nIn1, nIn2, nOut, reduction, reduction_prev):
        super(ConvDownNormal, self).__init__()
        self.reduction = reduction
        self.reduction_prev = reduction_prev
        nOutInternal = nOut
        self.conv_down = ConvBasic(nIn1, nOutInternal//2, 'down', self.reduction, self.reduction_prev)
        self.conv_normal = ConvBasic(nIn2, nOutInternal//2, 'norm', self.reduction, self.reduction_prev)

    def forward(self, x, y):
        res = [self.conv_down(x),
                self.conv_normal(y)]

        return torch.cat(res, dim=1)


class ClassifierV1(nn.Module):    
    def __init__(self, cell, channel, num_classes):
        super(ClassifierV1, self).__init__()
        self.conv = nn.AdaptiveAvgPool2d(1)
        self.linear = nn.Linear(channel, num_classes)

    def forward(self, x):
        res = self.conv(x)
        
        return self.linear(res.view(res.size(0), -1))
