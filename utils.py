#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    utils.py                                                          #
# Author:  Cristian Cioflan, adapted from Hanxiao Liu (arXiv:1806.09055)     #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          #
#                                                                            #
# The file contains functions and classes used as auxiliary tools to analyse #
# the performance of a network, to load/store it, to adapt the structure of  #
# a Cell, and to optimise the process of early exiting.                      #
#----------------------------------------------------------------------------#


import os
import numpy as np
import torch
import shutil
import torchvision.transforms as transforms
import torch
import torch.nn as nn
import math

from torch.autograd import Variable


def drop_path(x, drop_prob):
    if drop_prob > 0.:
        keep_prob = 1.-drop_prob
        mask = Variable(torch.cuda.FloatTensor(x.size(0), 1, 1, 1).bernoulli_(keep_prob))
        x.div_(keep_prob)
        x.mul_(mask)

    return x


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def count_parameters_in_MB(model):
    return np.sum(np.prod(v.size()) for name, v in model.named_parameters() if "auxiliary" not in name)/1e6

def save(model, model_path):
    torch.save(model.state_dict(), model_path)

def load(model, model_path):
    model.load_state_dict(torch.load(model_path))

def accuracy(output, target, topk=(1,)):
    """Computes the precision for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        # correct_k = correct[:k].view(-1).float().sum(0)
        correct_k = correct[:k].reshape(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))

    return res


class Tester(object):
    def __init__(self, model, args=None):
        self.args = args
        self.model = model.cuda()
        self.softmax = nn.Softmax(dim=1)

    def calc_logit(self, dataloader):
        self.model.eval()

        # nBlocks == number of stages
        n_stage = len(self.model.classifier)
        logits = [[] for _ in range(n_stage)]
        targets = []

        for i, (input, target) in enumerate(dataloader):
            targets.append(target)
            with torch.no_grad():
                input_var = torch.autograd.Variable(input).cuda()
                output = self.model(input_var)
                if not isinstance(output, list):
                    output = [output]
                # for each stage==block, compute the softmax at that point
                for b in range(n_stage):
                    _t = self.softmax(output[b])
                    logits[b].append(_t) 

            if i % self.args.print_freq == 0: 
                print('Generate Logit: [{0}/{1}]'.format(i, len(dataloader)))

        for b in range(n_stage):
            logits[b] = torch.cat(logits[b], dim=0)

        #size = (stages, samples, classes)
        size = (n_stage, logits[0].size(0), logits[0].size(1))
        ts_logits = torch.Tensor().resize_(size).zero_()
        for b in range(n_stage):
            ts_logits[b].copy_(logits[b])
        targets = torch.cat(targets, dim=0)
        ts_targets = torch.Tensor().resize_(size[1]).copy_(targets)

        return ts_logits, ts_targets

    def dynamic_eval_find_threshold(self, logits, targets, p, flops):
        """
            logits: m * n * c
            m: Stages
            n: Samples
            c: Classes
        """
        n_stage, n_sample, c = logits.size()
        max_preds, argmax_preds = logits.max(dim=2, keepdim=False)
        _, sorted_idx = max_preds.sort(dim=1, descending=True)
        filtered = torch.zeros(n_sample)
        T = torch.Tensor(n_stage).fill_(1e8)
        for k in range(n_stage - 1):
            acc, count = 0.0, 0
            # p grows with net's depth
            out_n = math.floor(n_sample * p[k])
            for i in range(n_sample):                        
                ori_idx = sorted_idx[k][i]
                if filtered[ori_idx] == 0:
                    # threshold value increases as a) you have more samples and/or b) you are in a later stage
                    count += 1
                    if count == out_n:
                        T[k] = max_preds[k][ori_idx]
                        break
            # Add number of elements larger than the found threshold
            filtered.add_(max_preds[k].ge(T[k]).type_as(filtered))

        T[n_stage -1] = -1e8  # accept all of the samples at the last stage
        acc_rec, exp = torch.zeros(n_stage), torch.zeros(n_stage)
        acc, expected_flops = 0, 0
        for i in range(n_sample):
            gold_label = targets[i]
            for k in range(n_stage):
                # compare to stage's threshold
                if max_preds[k][i].item() >= T[k]:  # force to exit at k
                    # if label==prediction, increase acc and acc[stage]
                    if int(gold_label.item()) == int(argmax_preds[k][i].item()):
                        acc += 1
                        acc_rec[k] += 1
                    exp[k] += 1
                    break
        acc_all = 0
        for k in range(n_stage):
            _t = 1.0 * exp[k] / n_sample
            expected_flops += _t * flops[k]
            acc_all += acc_rec[k]

        return acc * 100.0 / n_sample, expected_flops, T

    def dynamic_eval_with_threshold(self, logits, targets, flops, T):
        n_stage, n_sample, _ = logits.size()
        max_preds, argmax_preds = logits.max(dim=2, keepdim=False)  # take the max logits as confidence

        acc_rec, exp = torch.zeros(n_stage), torch.zeros(n_stage)
        acc, expected_flops = 0, 0
        for i in range(n_sample):
            gold_label = targets[i]
            for k in range(n_stage):
                if max_preds[k][i].item() >= T[k]:  # force to exit at k
                    _g = int(gold_label.item())
                    _pred = int(argmax_preds[k][i].item())
                    if _g == _pred:
                        acc += 1
                        acc_rec[k] += 1
                    exp[k] += 1
                    break
        acc_all, sample_all = 0, 0
        for k in range(n_stage):
            _t = exp[k] * 1.0 / n_sample
            sample_all += exp[k]
            expected_flops += _t * flops[k]
            acc_all += acc_rec[k]

        return acc * 100.0 / n_sample, expected_flops


