#----------------------------------------------------------------------------#
# Copyright (C) 2021, ETH Zurich, Switzerland                                #
# All rights reserved.                                                       #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License");            #
# you may not use this file except in compliance with the License.           #
# See LICENSE.apache.md in the top directory for details.                    #
#                                                                            #
#     http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# See the License for the specific language governing permissions and        #
#                                                                            #
# File:    visualise.py                                                      #
# Author:  Cristian Cioflan, adapted from Hanxiao Liu (arXiv:1806.09055)     #
# Date:    9.03.2021                                                         #
#                                                                            #
# File description:                                                          #
#                                                                            #
# The file provides the functions required to visualise the confusion matrix #
# obtained on a certain dataset and the structure of a searched Cell.        #                                   
#----------------------------------------------------------------------------#


import sys
import genotypes
import torch
import numpy
import seaborn
import pandas
import matplotlib as mpl
import matplotlib.pyplot as plt

from graphviz import Digraph


def confusion_matrix (dataloader, model, args):

    nb_classes = args.num_classes
    confusion_matrix = torch.zeros(nb_classes, nb_classes)
    with torch.no_grad():
        for i, (inputs, classes) in enumerate(dataloader):
            outputs = model(inputs)
            _, preds = torch.max(outputs[-1], 1)
            for t, p in zip(classes.view(-1), preds.view(-1)):
                confusion_matrix[t.long(), p.long()] += 1
       
    mpl.use('Agg')
    confusion_matrix_plot = numpy.array(confusion_matrix)
    confusion_matrix_plot = numpy.divide(confusion_matrix_plot, 10.0)
    
    df_cm = None
    df_cm = pandas.DataFrame(confusion_matrix_plot, range(nb_classes),
                  range(nb_classes))
    seaborn.set(font_scale=1.4)  # for label size
    seaborn.heatmap(df_cm, annot=True,annot_kws={"size": 10})  # font size
    plt.savefig('confusion_matrix.png')
    plt.clf()

def plot(genotype, filename):
    g = Digraph(
            format='pdf',
            edge_attr=dict(fontsize='10', fontname="times"),
            node_attr=dict(style='filled', shape='rect', align='center', fontsize='10', height='0.5', width='0.5', penwidth='2', fontname="times"),
            engine='dot')
    g.body.extend(['rankdir=LR'])

    g.node("c_{k-2}", fillcolor='darkseagreen2')
    g.node("c_{k-1}", fillcolor='darkseagreen2')
    assert len(genotype) % 2 == 0
    steps = len(genotype) // 2
    for i in range(steps):
        g.node(str(i), fillcolor='lightblue')
    for i in range(steps):
        for k in [2*i, 2*i + 1]:
            op, j = genotype[k]
            if j == 0:
                u = "c_{k-2}"
            elif j == 1:
                u = "c_{k-1}"
            else:
                u = str(j-2)
            v = str(i)
            g.edge(u, v, label=op, fillcolor="gray")
    g.node("c_{k}", fillcolor='palegoldenrod')
    for i in range(steps):
        g.edge(str(i), "c_{k}", fillcolor="gray")
    g.render(filename, view=True)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("usage:\n python {} ARCH_NAME".format(sys.argv))
        sys.exit(1)
    genotype_name = sys.argv[1]
    try:
        genotype = eval('genotypes.{}'.format(genotype_name))
    except AttributeError:
        print("{} is not specified in genotypes.py".format(genotype_name)) 
        sys.exit(1)
    plot(genotype.normal, "normal")
    plot(genotype.reduce, "reduction")

